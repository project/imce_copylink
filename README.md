# IMCE Copy Link

## DESCRIPTION

Copy URL of IMCE selected file into clipboard.

## INSTALLING

1.  To install the module copy the ‘imce_copylink’ folder to your
    sites/all/modules directory.

2.  Go to module administration page. Enable the module. Read more about
    installing modules at <http://drupal.org/node/70151>

## CONFIGURING AND USING

1.  Go to /admin/config/media/imce Assuming you have already created an
    IMCE profile. Under section ‘Configuration profiles’ click on
    approprate ‘Edit’ link.

2.  On the next page find section ‘Directories’. Set appropriate
    permission using the check box ‘Copy link’.

3.  Click on ‘Save configuration’ button.

4.  To test use IMCE. A new ‘Copy link’ tab button will be display.
    Select a file and click the ‘Copy link’ button. Then pasting the
    clipboard somewhere inserts the file link.

## UPGRADING

[Read more] at (http://drupal.org/node/250790)
